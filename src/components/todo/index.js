import { h, Component } from 'preact';
import { connect } from 'preact-redux';
import reduce from '../../reducers';
import * as actions from '../../actions';
import TodoItem from './todo-item';
import FormField from 'preact-material-components/FormField';
import 'preact-material-components/FormField/style.css';
import TextField from 'preact-material-components/TextField';
import 'preact-material-components/TextField/style.css';

@connect(reduce, actions)
export default class Todo extends Component {
	addTodos = () => {
		this.props.addTodo(this.state.text);
		this.setState({ text: '' });
	};

	removeTodo = (todo) => {
		this.props.removeTodo(todo);
	};

	updateText = (e) => {
		this.setState({ text: e.target.value });
	};

	render({ todos }, { text }) {
		return (
			<div id="todo">
				<form onSubmit={this.addTodos} action="javascript:">
					<FormField>
						<TextField label="New Item" value={text} onKeyUp={this.updateText} />
					</FormField>
				</form>
				<ul>
					{ todos.map(todo => (
						<TodoItem key={todo.id} todo={todo} onRemove={this.removeTodo} />
					)) }
				</ul>
			</div>
		);
	}
}